# Phalcon Skeleton
  Put together by Hudson Nicoletti.

## Whats this for?

This is a Skeleton for a simple, powerfull MVC Application that uses the Phalcon framework.

## Requirements

* PHP 8.1
* Phalcon >= 5.0.0
* Composer

### DEPENDENCIES
```shell
$ cd libraries
$ composer install
```
### REQUIREMENTS
  - Composer
  - LAMP
    - Apache
    - Mysql
    - PHP 8.1
  - [PhalconPHP v5](https://phalconphp.com/en/download)
  - Apache AllowOverride (mod_rewrite)
