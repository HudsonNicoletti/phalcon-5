<?php

namespace Api\Controllers;

use Phalcon\Mvc\Controller;

class LanguageController extends ControllerBase
{

  /*
  Checks users computer better designated language , if nothing matches it will fall back to english.
  */

  public function setBestLangauge()
  {
    $lang = substr($this->request->getBestLanguage(),0,2);

    $new_lang = $this->changeLanguage($lang);

    return $new_lang;
  }

  /*
    Falls back to English if nothing is defined and sets it as a cookie for storing
  */

  public function changeLanguage($language)
  {
    switch($language)
    {
      case 'pt': $this->cookies->set("website_lang","pt"); break;
      case 'en': $this->cookies->set("website_lang","en"); break;
      default:   $this->cookies->set("website_lang","en"); break;
    }

    return $this->cookies->get("website_lang")->getValue();
  }

  /*
  Since the appends the Action name ,this will be the action used by the routes that we have pointed to in the routes folder for manual
  language change
  */
  public function ChangeAction()
  {
    $n = $this->changeLanguage($this->dispatcher->getParam('language'));
    echo $this->cookies->get("website_lang")->getValue();
    $this->response->redirect('/');
  }

}
