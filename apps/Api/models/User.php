<?php

namespace Api\Models;


/*

The Class Name should be the same as the file name ( Case sensitive )
the ->setSource() shoud indicate what table on the mysql will be used.

*/


class User extends \Phalcon\Mvc\Model
{
  public function initialize()
  {
      $this->setSource("user");
  }
}
