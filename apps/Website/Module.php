<?php

namespace Website;

use Phalcon\Autoload\Loader,
    Phalcon\Mvc\View,
    Phalcon\Config\Config\Adapter\Ini,
    Phalcon\Mvc\ModuleDefinitionInterface,
    Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

class Module
{
  public function registerAutoloaders()
  {
    $loader = new Loader();

    $loader->setNamespaces([
      'Website\Controllers' => __DIR__ . '/controllers/',

      // using namespace for API folder so we dont need to have multiple copies for one application
      'Api\Controllers'     => __DIR__ . '/../Api/controllers/',
      'Api\Models'          => __DIR__ . '/../Api/models/',
    ]);

    $loader->register();
  }

  public function registerServices($di)
  {

    $di['view'] = function() {
      $view = new View;
      $view->setViewsDir(__DIR__ . '/views/');

      return $view;
    };

  }
}
