<?php

namespace Website\Controllers;

use Phalcon\Mvc\Controller;

use Api\Controllers\LanguageController as Language;


class ControllerBase extends Controller
{
  public $language = false;

  public function initialize()
  {
    $this->assets
    ->addCss('https://fonts.googleapis.com/css2?family=Hind:wght@400;700&amp;family=Manrope:wght@200;300;400;600;700;800&amp;display=swap',false);


    /*
      Sets Global Variable "language" accessable for every controller with current cookie or if new user will generate a new one from the Api Controller
    */

    $this->language = ($this->cookies->has("website_lang") ? $this->cookies->get("website_lang")->getValue() : (new Language())->setBestLangauge());
  }

}
