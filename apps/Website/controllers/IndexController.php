<?php

namespace Website\Controllers;

use Phalcon\Mvc\View;

class IndexController extends ControllerBase
{

  public function IndexAction()
  {
    /* Selects the view based on the defined variable set by the cookie*/
    $this->view->pick("index/{$this->language}/index");
  }

}
