<?php
/**
  * this file is calling for all routes in the routes folder for better seperation.
  *
**/
$files = glob( __DIR__ . '/routes/*.php');

foreach ($files as $file) {
    require($file);
}

# This redirects user to a server error page if debug is set to false in the config.json and public/index.php
$router->add("/error/ServerError", [
  "namespace"  => "Website\Controllers",
  "module"     => "Website",
  'controller' => 'index',
  'action'     => 'terms'
]);
