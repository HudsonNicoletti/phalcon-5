<?php

# Changes website view to use the EN folder for multi language support
$router->add("/english", [
  "namespace"  => "Api\Controllers",
  "module"     => "Api",
  'controller' => 'Language',
  'action'     => 'change',
  'language'   => 'en'
]);

# Changes website view to use the PT folder for multi language support
$router->add("/portuguese", [
  "namespace"  => "Api\Controllers",
  "module"     => "Api",
  'controller' => 'Language',
  'action'     => 'change',
  'language'   => 'pt'
]);
