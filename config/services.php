<?php

/**
  * Here is declared all Phalcon Needs to function
**/

use Phalcon\Mvc\Dispatcher\Exception as DispatchException,
    Phalcon\Session\Adapter\Stream as SessionAdapter,
    Phalcon\Session\Manager as SessionManager,
    Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter,
    Phalcon\Mvc\Dispatcher as PhDispatcher,
    Phalcon\Events\Manager as EventsManager,
    Phalcon\Mvc\Url as UrlResolver,
    Phalcon\Config\Config\Adapter\Ini,
    Phalcon\Di\FactoryDefault,
    Phalcon\Mvc\Router,
    Phalcon\Di\Di\Dispatcher;

use PHPMailer\PHPMailer\PHPMailer;

/**
  * Generates a new Default && grabs the config.json data
**/
$di = new FactoryDefault();
$cf = (Object)json_decode(file_get_contents( __DIR__ . "/../config/config.json"));

/**
  * Here we initiate the database configuration
**/
$di['db'] = function() use ($cf) {

    return new DbAdapter([
      "host"      => $cf->database->host,
      "username"  => $cf->database->username,
      "password"  => $cf->database->password,
      "dbname"    => $cf->database->dbname,
      "charset"   => $cf->database->charset
    ]);

};

#    The URL component is used to generate all kinds of URLs in the application
$di['url'] = function () {
    $url = new UrlResolver();
    $url->setBaseUri('/');

    return $url;
};

#   Starts the session the first time some component requests the session service
$di['session'] = function (){
  $session = new SessionManager();
  $files = new SessionAdapter([
      'savePath' => sys_get_temp_dir(),
  ]);
  $session->setAdapter($files);
  $session->start();

  return $session;
};

# Cookies & crypt need eachother to function.
$di['cookies'] = function() {
    $cookies = new Phalcon\Http\Response\Cookies();
    $cookies->useEncryption(false);
    return $cookies;
};

$di['crypt'] = function() use ($cf) {

    $crypt = new Phalcon\Encryption\Crypt();

    return $crypt;
};

/**
  * RETURNS SOME DATA FROM CONFIG.JSON TO controller via $this->configuration
**/
$di['configuration'] = function () use ($cf) {
    return (object)[
      "database"      => $cf->database ,
      "mail"          => $cf->mail,
      "debug"         => $cf->server->debug,
    ];
};


#   Handles 404
$di['dispatcher'] = function () {
    $eventsManager = new EventsManager();
    $eventsManager->attach("dispatch:beforeException", function($event, $dispatcher, $exception) {

      if ($exception instanceof DispatchException) {
        $dispatcher->forward(array(
          'controller' => 'index',
          'action'     => 'notfound'
        ));
        return false;
      }

    });

    $dispatcher = new PhDispatcher();
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
};

#   Configure PHPMailer ( loaded by composer ) , returning the mail ini config & PHPMailer functions
$di['mail'] = function () use ($cf) {
    $mail = new PHPMailer(true);

    $mail->isSMTP();
    $mail->isHTML(true);
    $mail->SMTPAuth = true;

    $mail->CharSet      = $cf->mail->charset;
    $mail->Host         = $cf->mail->host;
    $mail->SMTPAuth     = true;
    $mail->From         = $cf->mail->username;
    $mail->Username     = $cf->mail->username;
    $mail->Password     = $cf->mail->password;
    $mail->SMTPSecure   = $cf->mail->security;
    $mail->Port         = $cf->mail->port;

    return $mail;
};

#    Registering a router
$di['router'] = function ()  use ($di){
    $router = new Router();

    $session = $di['session'];
    $active_session = $session->has("strong_session");

    $router->setDefaultModule('Website');
    $router->setDefaultNamespace('Website\Controllers');
    $router->removeExtraSlashes(true);

    require( __DIR__ . "/routes.php" );

    return $router;
};
